//
//  UtilityMethods.m
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/13/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "UtilityMethods.h"

@implementation UtilityMethods

static UtilityMethods *_singleton = nil;

+ (UtilityMethods *)singleton
{
    if(!_singleton)
    {
        _singleton = [[UtilityMethods alloc] init];
    }
    return _singleton;
}

- (void)logFileSize
{
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[self.fileURL path] error:nil];
    
    NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
    long long fileSize = [fileSizeNumber longLongValue];
    
    NSLog(@"filesize: %lld B", fileSize);
}

@end
