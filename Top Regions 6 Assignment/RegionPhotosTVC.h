//
//  PlacePhotosTVC.h
//  Top Places 5 Assignment
//
//  Created by Admin on 7/7/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotosTVC.h"
#import "Region.h"

@interface RegionPhotosTVC : PhotosTVC
@property (strong, nonatomic) Region *region;
@end
