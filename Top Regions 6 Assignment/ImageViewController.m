//
//  ImageViewController.m
//  Imaginarium
//
//  Created by Admin on 6/24/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController () <UIScrollViewDelegate, NSURLSessionDownloadDelegate, UISplitViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *downloadingImageLabel;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImageView *imageView;
@property (nonatomic) BOOL needToShowDownloadingProgress;
@end

@implementation ImageViewController

#pragma mark - Properties

- (void)setScrollView:(UIScrollView *)scrollView
{
    _scrollView = scrollView;
    _scrollView.delegate = self;
    self.scrollView.contentSize = self.image ? self.image.size : CGSizeZero;
}

- (UIImageView *)imageView
{
    if(!_imageView)
        _imageView = [[UIImageView alloc] init];
    return _imageView;
}


- (UIImage *)image
{
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image
{
    [self hideDownloadingProgressViews];
    self.scrollView.zoomScale = 1.0;
    
    self.imageView.image = image;
    self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    
    self.scrollView.contentSize = self.image ? self.image.size : CGSizeZero;
    
    [self updateScrollViewZoomBounds];
}

- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    [self startDownloadingImage];
}

#pragma mark - Updating UI

- (void)startDownloadingImage
{
    self.image = nil;
    if (self.imageURL) {
        self.progressView.progress = 0.0;
        self.needToShowDownloadingProgress = YES;
        [self showDownloadingProgressViews];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:self.imageURL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request];
        [task resume];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.scrollView addSubview:self.imageView];
    [self showDownloadingProgressViews];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [self updateScrollViewZoomBounds];
}

- (void)setNeedToShowDownloadingProgress:(BOOL)needToShowDownloadingProgress
{
    _needToShowDownloadingProgress = needToShowDownloadingProgress;
    if (_needToShowDownloadingProgress) {
        [self showDownloadingProgressViews];
    } else {
        [self hideDownloadingProgressViews];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)showDownloadingProgressViews
{
    if (self.needToShowDownloadingProgress) {
        self.downloadingImageLabel.hidden = NO;
        self.progressView.hidden = NO;
    }
}

- (void)hideDownloadingProgressViews
{
    if (!self.needToShowDownloadingProgress) {
        self.downloadingImageLabel.hidden = YES;
        self.progressView.hidden = YES;
    }
}

- (void)updateScrollViewZoomBounds
{
    self.scrollView.minimumZoomScale = MIN(1.0, MIN(self.scrollView.bounds.size.width / self.image.size.width, self.scrollView.bounds.size.height / self.image.size.height));
    self.scrollView.maximumZoomScale = 3.0;
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.progressView setProgress:(double)totalBytesWritten / (double)totalBytesExpectedToWrite animated:YES];
    });
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)localfile
{
    if([downloadTask.currentRequest.URL isEqual:self.imageURL]) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:localfile]];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image = image;
            self.needToShowDownloadingProgress = NO;
        });
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    
}

#pragma mark - UISplitViewControllerDelegate

- (void)awakeFromNib
{
    self.splitViewController.delegate = self;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}

- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = @"Menu";
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    self.navigationItem.leftBarButtonItem = nil;
}

@end
