//
//  AppDelegate.m
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/9/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "AppDelegate.h"
#import "ManagedObjectContextAvailabitlity.h"
#import "UtilityMethods.h"
#import "FlickrFetcher.h"
#import "Photo+Flickr.h"
#import <CoreData/CoreData.h>

@interface AppDelegate () <NSURLSessionDownloadDelegate>
@property (strong, nonatomic) UIManagedDocument *managedDocument;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSURLSession *flickrDownloadSession;
@property (strong, nonatomic) NSTimer *foregroundFlickrFetchTimer;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundInitTaskIdentifier;
@property (copy, nonatomic) void (^flickrDownloadBackgroundURLSessionCompletionHandler)();
@end

@implementation AppDelegate

#define FLICKR_SESSION @"Flickr download session"
#define FLICKR_FETCH @"Flickr top regions fetch"
#define BACKGROUND_FETCH_INTERVAL_LIMIT (10)
#define FLICKR_FOREGROUND_FETCH_INTERVAL (20 * 60)

#define LAST_ERASED_KEY @"Last erased date"

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self beginBackgroundInitilizationTask];
    [self prepareManagedDocument];
    return YES;
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"BG fetch initialized");
    if(self.managedObjectContext)
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        sessionConfiguration.allowsCellularAccess = NO;
        sessionConfiguration.timeoutIntervalForRequest = BACKGROUND_FETCH_INTERVAL_LIMIT;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        NSURLRequest *request = [NSURLRequest requestWithURL:[FlickrFetcher URLforRecentGeoreferencedPhotos]];
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request
        completionHandler:^(NSURL *localfile, NSURLResponse *response, NSError *error)
        {
            if(error)
            {
                NSLog(@"Flickr background fetch failed: %@", error.localizedFailureReason);
                completionHandler(UIBackgroundFetchResultNoData);
            }
            else
            {
                [self loadPhotosFromLocalURL:localfile
                    intoManagedObjectContext:self.managedObjectContext
                             andExecuteBlock:^
                             {
                                 completionHandler(UIBackgroundFetchResultNewData);
                                 NSLog(@"BG fetch completed");
                             }];
            }
        }];
        [task resume];
    }
    else
    {
        NSLog(@"MOC is not ready for BG fetch");
        completionHandler(UIBackgroundFetchResultNoData);
    }
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    self.flickrDownloadBackgroundURLSessionCompletionHandler = completionHandler;
}

#pragma mark - NSURLSessionDownloadDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)localfile
{
    if([downloadTask.taskDescription isEqualToString:FLICKR_FETCH]) {
        [self loadPhotosFromLocalURL:localfile intoManagedObjectContext:self.managedObjectContext andExecuteBlock:^{
             [self flickrDownloadTasksMightBeComplete];
         }];
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if(error && (session == self.flickrDownloadSession)) {
        NSLog(@"Flickr background download session failed: %@", error.localizedDescription);
        [self flickrDownloadTasksMightBeComplete];
    }
}

- (void)flickrDownloadTasksMightBeComplete
{
    if(self.flickrDownloadBackgroundURLSessionCompletionHandler) {
        [self.flickrDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks,
                                                                    NSArray *uploadTasks,
                                                                    NSArray *downloadTasks) {
             if(![downloadTasks count]) {
                 void (^completionHandler)() = self.flickrDownloadBackgroundURLSessionCompletionHandler;
                 self.flickrDownloadBackgroundURLSessionCompletionHandler = nil;
                 if(completionHandler)
                     completionHandler();
             } else {
                 for(NSURLSessionDownloadTask *task in downloadTasks)
                     [task resume];
             }
         }];
    }
}


#pragma mark - Properties

- (NSURLSession *)flickrDownloadSession
{
    if(!_flickrDownloadSession) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:FLICKR_SESSION];
            _flickrDownloadSession = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
        });
    }
    return _flickrDownloadSession;
}

#pragma mark - Updating database

- (void)loadPhotosFromLocalURL:(NSURL *)localURL
      intoManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
               andExecuteBlock:(void(^)())whenDone
{
    if(managedObjectContext) {
        NSArray *photos = [self flickrPhotosAtURL:localURL];
        [managedObjectContext performBlock:^{
            [Photo insertPhotosWithFlickrDictionaries:photos inManagedObjectContext:managedObjectContext];
        }];
    }
    if(whenDone)
        whenDone();
}
#pragma mark - Getting data from Flickr

- (NSArray *)flickrPhotosAtURL:(NSURL *)url
{
    NSData *jsonResults = [NSData dataWithContentsOfURL:url];
    NSDictionary *propertyListResults = [NSJSONSerialization JSONObjectWithData:jsonResults options:0 error:NULL];
    return [propertyListResults valueForKeyPath:FLICKR_RESULTS_PHOTOS];
}

- (void)startFlickrFetch:(NSTimer *)timer
{
    [self startFlickrFetch];
}

- (void)startFlickrFetch
{
    [self.flickrDownloadSession getTasksWithCompletionHandler:^(NSArray *dataTasks,
                                                                NSArray *uploadTasks,
                                                                NSArray *downloadTasks) {
        if(![downloadTasks count]) {
            NSLog(@"new startFlickrFetch task started!");
            NSURLSessionDownloadTask *task = [self.flickrDownloadSession downloadTaskWithURL:[FlickrFetcher URLforRecentGeoreferencedPhotos]];
            task.taskDescription = FLICKR_FETCH;
            [task resume];
        } else {
            for (NSURLSessionDownloadTask *task in downloadTasks)
                [task resume];
        }
    }];
}

#pragma mark - UIManagedDocument and its NSManagedObjectContext

- (NSManagedObjectContext *)managedObjectContext
{
    return self.managedDocument.managedObjectContext;
}

- (void)prepareManagedDocument
{
    NSURL *url = [self managedDocumentURL];
    [UtilityMethods singleton].fileURL = url;
    
    UIManagedDocument *managedDocument = [[UIManagedDocument alloc] initWithFileURL:url];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        [managedDocument openWithCompletionHandler:^(BOOL success) {
            if(success) {
                [[UtilityMethods singleton] logFileSize];
                NSLog(@"Document exists and is ready!");
                [self documentIsReady:managedDocument];
            } else {
                NSLog(@"Cannot open the document at URL: %@", url);
            }
            
            [self endBackgroundInitializationTask];
        }];
    }
    else {
        [managedDocument saveToURL:url
                  forSaveOperation:UIDocumentSaveForCreating
        completionHandler:^(BOOL success) {
            if (success) {
                [[UtilityMethods singleton] logFileSize];
                NSLog(@"Document new and is ready!");
                [self documentIsReady:managedDocument];
            } else {
                NSLog(@"Cannot create the document at URL: %@", url);
            }
            
            [self endBackgroundInitializationTask];
        }];
    }
}

- (void)beginBackgroundInitilizationTask
{
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    self.backgroundInitTaskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^
                                         {
                                             [self endBackgroundInitializationTask];
                                             NSLog(@"BG initialization time expired");
                                         }];
}

- (void)endBackgroundInitializationTask
{
    [[UIApplication sharedApplication] endBackgroundTask:self.backgroundInitTaskIdentifier];
    self.backgroundInitTaskIdentifier = UIBackgroundTaskInvalid;
}

- (void)documentIsReady:(UIManagedDocument *)managedDocument
{
    if(managedDocument.documentState == UIDocumentStateNormal) {
        self.managedDocument = managedDocument;
        
        [self.foregroundFlickrFetchTimer invalidate];
        self.foregroundFlickrFetchTimer = nil;
        
        if(self.managedObjectContext) {
            [self removeOldDatabaseContentsInManagedObjectContext:self.managedObjectContext];
            
            self.foregroundFlickrFetchTimer = [NSTimer timerWithTimeInterval:FLICKR_FOREGROUND_FETCH_INTERVAL
                                                                      target:self
                                                                    selector:@selector(startFlickrFetch:)
                                                                    userInfo:nil
                                                                     repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:self.foregroundFlickrFetchTimer forMode:NSDefaultRunLoopMode];
        }
        
        NSDictionary *userInfo = @{ManagedObjectContext : managedDocument.managedObjectContext};
        [[NSNotificationCenter defaultCenter] postNotificationName:ManagedObjectContextAvailability object:self userInfo:userInfo];
    } else {
        [self prepareManagedDocument];
    }
}

- (NSURL *)managedDocumentURL
{
    return [[self applicationDirectoryURL] URLByAppendingPathComponent:@"TopRegions.md"];
}

- (NSURL *)applicationDirectoryURL
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
}

#pragma mark - Checking database for expiration date

- (void)removeOldDatabaseContentsInManagedObjectContext:(NSManagedObjectContext *)context
{
    NSUserDefaults *standartUserDefaults = [NSUserDefaults standardUserDefaults];
    NSDate *lastErasedDate = [standartUserDefaults objectForKey:LAST_ERASED_KEY];
    if(lastErasedDate) {
        if([lastErasedDate timeIntervalSinceNow] < -7 * 24 * 60 * 60) { // a week
            NSFetchRequest *regionsRequest = [[NSFetchRequest alloc] initWithEntityName:@"Region"];
            
            NSError *error;
            NSArray *regionsToRemove = [context executeFetchRequest:regionsRequest error:&error];
            if(!regionsToRemove) {
                NSLog(@"Cannot erase database: %@", [error localizedFailureReason]);
            } else {
                for (NSManagedObject *managedObject in regionsToRemove)
                    [context deleteObject:managedObject];
                NSLog(@"LAST_ERASED_KEY have been REset!");
                [standartUserDefaults setObject:[NSDate date] forKey:LAST_ERASED_KEY];
            }
        }
    } else {
        NSLog(@"LAST_ERASED_KEY have been set!");
        [standartUserDefaults setObject:[NSDate date] forKey:LAST_ERASED_KEY];
    }
}

@end
