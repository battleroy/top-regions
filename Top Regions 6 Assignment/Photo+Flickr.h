//
//  Photo+Flickr.h
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/10/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

+ (Photo *)photoWithFlickrDictionary:(NSDictionary *)dictionary
              inManagedObjectContext:(NSManagedObjectContext *)context;

+ (void)insertPhotosWithFlickrDictionaries:(NSArray *)dictionaries inManagedObjectContext:(NSManagedObjectContext *)context;

@end
