//
//  PhotosTVC.m
//  Top Places 5 Assignment
//
//  Created by Admin on 7/8/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "PhotosTVC.h"
#import "ImageViewController.h"
#import "Photo.h"

@interface PhotosTVC ()

@end

@implementation PhotosTVC

#pragma mark - Properties

- (NSFetchedResultsController *)createFetchedResultsController
{
    return nil;
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    self.fetchedResultsController = [self createFetchedResultsController];
}

#pragma mark - Updating UI

- (IBAction)performFetch
{
    [self.refreshControl beginRefreshing];
    [super performFetch];
    [self.refreshControl endRefreshing];
}

- (void)beginRefreshingSpinner
{
    [self.refreshControl beginRefreshing];
    [self.tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
}

- (void)endRefreshingSpinner
{
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.refreshControl endRefreshing];
}

#pragma mark - UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photos Cell" forIndexPath:indexPath];
    
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (![photo.title length]) {
        cell.textLabel.text = [photo.subtitle length] ? photo.subtitle : @"Unknown";
    } else {
        cell.textLabel.text = photo.title;
    }
    cell.detailTextLabel.text = photo.subtitle;
    cell.imageView.image = [UIImage imageWithData:photo.thumbnailImage];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (!photo.thumbnailImage)
        [self updatePhotoThumbnailImage:photo];
    [self updateLastViewedDateForPhoto:photo];
    [self updateDetailViewControllerWithPhoto:photo];
}

#pragma mark - Navigation


- (void)updateLastViewedDateForPhoto:(Photo *)photo
{
    photo.lastViewedDate = [NSDate date];
}

- (void)updateDetailViewControllerWithPhoto:(Photo *)photo
{
    id detailVC = self.splitViewController.viewControllers[1];
    if([detailVC isKindOfClass:[UINavigationController class]])
        detailVC = [((UINavigationController *)detailVC).viewControllers firstObject];
    if ([detailVC isKindOfClass:[ImageViewController class]])
        [self prepareImageViewController:detailVC toDisplayPhoto:photo];
}


- (void)updatePhotoThumbnailImage:(Photo *)photo
{
    dispatch_queue_t downloadQueue = dispatch_queue_create("download thumbnail", NULL);
    dispatch_async(downloadQueue, ^{
       NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:photo.thumbnailURL]];
       NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
       NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
       
       NSURLSessionDownloadTask *task = [session
         downloadTaskWithRequest:urlRequest
         completionHandler:^(NSURL *localfile, NSURLResponse *response, NSError *error) {
             dispatch_async(dispatch_get_main_queue(),^{
                photo.thumbnailImage = [NSData dataWithContentsOfURL:localfile];
                NSLog(@"Downloading thumbnail finished, nil? %@", (photo.thumbnailImage ? @"NO" : @"YES"));
                [self performFetch];
            });
         }];
       
       NSLog(@"Downloading thumbnail started");
       [task resume];
   });
}

- (void)prepareImageViewController:(ImageViewController *)ivc toDisplayPhoto:(Photo *)photo
{
    ivc.imageURL = [NSURL URLWithString:photo.imageURL];
    ivc.title = photo.title;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    if([segue.identifier isEqualToString:@"View Photo"]) {
        if([segue.destinationViewController isKindOfClass:[ImageViewController class]]) {
            ImageViewController *ivc = (ImageViewController *)segue.destinationViewController;
            
            Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
            [self prepareImageViewController:ivc toDisplayPhoto:photo];
        }
    }
}

@end
