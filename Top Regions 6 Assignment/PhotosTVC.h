//
//  PhotosTVC.h
//  Top Places 5 Assignment
//
//  Created by Admin on 7/8/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController/CoreDataTableViewController.h"

@interface PhotosTVC : CoreDataTableViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (NSFetchedResultsController *)createFetchedResultsController;
- (void)beginRefreshingSpinner;
- (void)endRefreshingSpinner;

@end
