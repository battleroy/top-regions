//
//  RecentPhotosTVC.m
//  Top Places 5 Assignment
//
//  Created by Admin on 7/8/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "RecentPhotosTVC.h"
#import "ManagedObjectContextAvailabitlity.h"

@interface RecentPhotosTVC ()

@end

@implementation RecentPhotosTVC

#pragma mark - Initialization

- (void)awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserverForName:ManagedObjectContextAvailability
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note)
     {
         self.managedObjectContext = note.userInfo[ManagedObjectContext];
         NSLog(@"-----RPTVC-----   MOC is READY!");
     }];
}

#pragma mark - Properties

- (NSFetchedResultsController *)createFetchedResultsController
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Photo"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"lastViewedDate" ascending:NO]];
    request.predicate = [NSPredicate predicateWithFormat:@"lastViewedDate != nil"];
    request.fetchLimit = 20;
    
    NSFetchedResultsController *nsfrc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    return nsfrc;
}


@end
