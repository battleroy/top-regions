//
//  TopPlacesTVC.m
//  Top Places 5 Assignment
//
//  Created by Admin on 7/7/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "TopRegionsTVC.h"
#import "FlickrFetcher.h"
#import "RegionPhotosTVC.h"
#import "ManagedObjectContextAvailabitlity.h"
#import "Photo+Flickr.h"
#import "Region.h"
#import "UtilityMethods.h"
#import "AppDelegate.h"

@interface TopRegionsTVC ()

@end

@implementation TopRegionsTVC

#pragma mark - Initilalization

- (void)awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserverForName:ManagedObjectContextAvailability
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note)
                                                 {
                                                     self.managedObjectContext = note.userInfo[ManagedObjectContext];
                                                     NSLog(@"-----TRTVC-----    MOC is READY!");
                                                 }];
}

#pragma mark - Properties

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Region"];
    request.predicate = nil;
    request.fetchLimit = 50;
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"photosCount" ascending:NO]];
    
    NSFetchedResultsController *nsfrc = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                            managedObjectContext:managedObjectContext sectionNameKeyPath:nil
                                                                                       cacheName:nil];
    self.fetchedResultsController = nsfrc;
}

#pragma mark - Updating UI

- (IBAction)fetchTopRegions
{
    [self beginRefreshingSpinner];
    [self performFetch];
    [self endRefreshingSpinner];
}

- (void)beginRefreshingSpinner
{
    [self.refreshControl beginRefreshing];
    [self.tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
}

- (void)endRefreshingSpinner
{
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    [self.refreshControl endRefreshing];
}

#pragma mark UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Top Region Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    Region *region = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = region.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:([region.photosCount intValue] == 1 ? @"%@ photo" : @"%@ photos"), region.photosCount];
    
    return cell;
}

#pragma mark - Fetching
- (void)performFetch
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] startFlickrFetch];
    [super performFetch];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"Photos For Region"]) {
        RegionPhotosTVC *rptvc = nil;
        Region *selectedRegion = [self.fetchedResultsController objectAtIndexPath:[self.tableView indexPathForCell:sender]];
        
        if([segue.destinationViewController isKindOfClass:[RegionPhotosTVC class]]) {
            rptvc = (RegionPhotosTVC *)segue.destinationViewController;
        } else if ([segue.destinationViewController isKindOfClass:[UISplitViewController class]]) {
            id master = ((UISplitViewController *)segue.destinationViewController).viewControllers[0];
            if([master isKindOfClass:[RegionPhotosTVC class]])
                rptvc = master;
        }
        
        rptvc.region = selectedRegion;
        rptvc.title = selectedRegion.name;
    }
}

@end
