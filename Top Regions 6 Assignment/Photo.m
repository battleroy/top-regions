//
//  Photo.m
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/13/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Photo.h"
#import "Region.h"


@implementation Photo

@dynamic imageURL;
@dynamic photoId;
@dynamic subtitle;
@dynamic thumbnailImage;
@dynamic thumbnailURL;
@dynamic title;
@dynamic lastViewedDate;
@dynamic region;

@end
