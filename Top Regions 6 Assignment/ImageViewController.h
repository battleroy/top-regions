//
//  ImageViewController.h
//  Imaginarium
//
//  Created by Admin on 6/24/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (strong, nonatomic) NSURL *imageURL;

@end
