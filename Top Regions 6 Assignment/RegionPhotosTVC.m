//
//  PlacePhotosTVC.m
//  Top Places 5 Assignment
//
//  Created by Admin on 7/7/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "RegionPhotosTVC.h"

@interface RegionPhotosTVC ()
@end

@implementation RegionPhotosTVC

#pragma mark - Properties

- (void)setRegion:(Region *)region
{
    _region = region;
    self.managedObjectContext = region.managedObjectContext;
}

- (NSFetchedResultsController *)createFetchedResultsController
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Photo"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"region = %@", self.region];
    
    NSFetchedResultsController *nsfrc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    return nsfrc;
}

@end
