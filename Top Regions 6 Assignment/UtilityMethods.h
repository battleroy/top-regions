//
//  UtilityMethods.h
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/13/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilityMethods : NSObject

+ (UtilityMethods *)singleton;

@property (strong, nonatomic) NSURL *fileURL;

- (void)logFileSize;

@end
