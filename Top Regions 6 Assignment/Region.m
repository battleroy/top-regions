//
//  Region.m
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/13/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Region.h"
#import "Photo.h"


@implementation Region

@dynamic name;
@dynamic photosCount;
@dynamic photos;

@end
