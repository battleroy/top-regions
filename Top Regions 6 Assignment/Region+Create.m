//
//  Region+Create.m
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/10/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Region+Create.h"
#import "FlickrFetcher.h"

@implementation Region (Create)

+ (Region *)regionForName:(NSString *)regionName inManagedObjectContext:(NSManagedObjectContext *)context
{
    Region *region = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Region"];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", regionName];
    
    NSError *error;
    NSArray *fetchResults = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Error while getting region from database: %@", [error localizedFailureReason]);
    } else if ([fetchResults count] > 1) {
        NSLog(@"There are more than one region with \"UNIQUE\" name");
    } else if ([fetchResults count]) {
        region = [fetchResults firstObject];
    } else {
        region = [NSEntityDescription insertNewObjectForEntityForName:@"Region" inManagedObjectContext:context];
        region.name = regionName;
    }
    
    return region;
}

@end
