//
//  AppDelegate.h
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/9/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)startFlickrFetch;

@end
