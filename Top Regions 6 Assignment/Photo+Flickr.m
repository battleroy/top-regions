//
//  Photo+Flickr.m
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/10/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Photo+Flickr.h"
#import "FlickrFetcher.h"
#import "Region+Create.h"

@implementation Photo (Flickr)

+ (Photo *)photoWithFlickrDictionary:(NSDictionary *)dictionary
              inManagedObjectContext:(NSManagedObjectContext *)context
{
    Photo *photo = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Photo"];
    request.predicate = [NSPredicate predicateWithFormat:@"photoId = %@", [dictionary valueForKeyPath:FLICKR_PHOTO_ID]];
    
    NSError *error;
    NSArray *fetchResults = [context executeFetchRequest:request error:&error];
    if(error) {
        NSLog(@"Error while getting photo from database: %@", [error localizedFailureReason]);
    } else if ([fetchResults count] > 1) {
        NSLog(@"There are more than one photo with \"UNIQUE\" ID");
    } else if ([fetchResults count]) {
        photo = [fetchResults firstObject];
    } else {
        photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
        photo.title = [dictionary valueForKeyPath:FLICKR_PHOTO_TITLE];
        photo.subtitle = [dictionary valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
        photo.thumbnailURL = [[FlickrFetcher URLforPhoto:dictionary format:FlickrPhotoFormatSquare] absoluteString];
        photo.imageURL = [[FlickrFetcher URLforPhoto:dictionary format:FlickrPhotoFormatLarge] absoluteString];
        photo.photoId = [dictionary valueForKeyPath:FLICKR_PHOTO_ID];
        
        dispatch_queue_t downloadQueue = dispatch_queue_create("download photo info", NULL);
        dispatch_async(downloadQueue,^{
            NSURL *urlForInformationAboutPlace = [FlickrFetcher URLforInformationAboutPlace:[dictionary valueForKeyPath:FLICKR_PHOTO_PLACE_ID]];
            NSData *jsonData = [NSData dataWithContentsOfURL:urlForInformationAboutPlace];
            if(jsonData) {
                NSDictionary *placeInformation = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:NULL];
                dispatch_async(dispatch_get_main_queue(),^{
                    photo.region = [Region regionForName:[FlickrFetcher extractRegionNameFromPlaceInformation:placeInformation]
                                  inManagedObjectContext:context];
                    NSInteger prevCount = [photo.region.photosCount intValue];
                    photo.region.photosCount = [NSNumber numberWithInteger:prevCount + 1];
                });
            }
        });
    }
    
    return photo;
}

+ (void)insertPhotosWithFlickrDictionaries:(NSArray *)dictionaries inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSLog(@"photos count: %lu", (unsigned long)[dictionaries count]);
    for (NSDictionary *photoDictionary in dictionaries)
        [Photo photoWithFlickrDictionary:photoDictionary inManagedObjectContext:context];
}

@end
