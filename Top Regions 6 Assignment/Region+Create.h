//
//  Region+Create.h
//  Top Regions 6 Assignment
//
//  Created by Admin on 7/10/15.
//  Copyright (c) 2015 CS193p. All rights reserved.
//

#import "Region.h"

@interface Region (Create)

+ (Region *)regionForName:(NSString *)regionName inManagedObjectContext:(NSManagedObjectContext *)context;

@end
